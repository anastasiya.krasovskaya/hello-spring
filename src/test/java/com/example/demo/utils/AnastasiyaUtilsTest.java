package com.example.demo.utils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AnastasiyaUtilsTest {

    @Test
    public void test() {
        Integer a = 5;
        Integer b = 6;
        Integer expected = a+b;
        Integer sum_a_plus_b = AnastasiyaUtils.varyGookSumm(a, b);
        System.out.println("" + a + " + " + b + " = " + sum_a_plus_b) ;
        System.out.println("expected: " + expected) ;
        assertEquals(expected, sum_a_plus_b);
    }


}
